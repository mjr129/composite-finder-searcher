"""
Usage:

composite-finder-searcher [--min|--max|--avg|--sum] [<quantity>] [<file_name>]

Searches a composite finder file for composites with a cerain number of sequences per family.
    min: Filter by smallest family
    max: Filter by largest family
    avg: Filter by average family
    sum: Filter by total number of sequences (regardless of family)
"""

from sys import argv


FILENAME = 1
QUANTITY = 2

file_name = None
ncmd = FILENAME
mode = None
quantity = 10


def avg( x ):
    return sum( x ) / len( x )


if len( argv ) == 1 or "--help" in argv or "/?" in argv:
    print( __doc__ )
    exit( 1 )

for x in argv[ 1: ]:
    if ncmd == FILENAME:
        if x == "--min" or x == "/min":
            assert mode is None, "Mode specified more than once."
            mode = min
            ncmd = QUANTITY
        elif x == "--max" or x == "/max":
            assert mode is None, "Mode specified more than once."
            mode = max
            ncmd = QUANTITY
        elif x == "--avg" or x == "/avg":
            assert mode is None, "Mode specified more than once."
            mode = avg
            ncmd = QUANTITY
        elif x == "--sum" or x == "/sum":
            assert mode is None, "Mode specified more than once."
            mode = sum
            ncmd = QUANTITY
        else:
            assert file_name is None, "Filename specified more than once."
            file_name = x
    elif ncmd == QUANTITY:
        quantity = int( x )
        ncmd = FILENAME

assert file_name, "Filename not specified"
assert mode, "Mode not specified"
assert quantity, "Quantity not specified"
comp_name = None
family_sizes = [ ]

with open( file_name, "r" ) as file:
    for line in file:
        if line.startswith( ">C" ):
            if comp_name:
                mimumum = mode( family_sizes )  # type:int
                
                if mimumum > quantity:
                    print( "{} = {}".format( comp_name, ", ".join( str( x ) for x in family_sizes ) ) )
            
            comp_name = line
            num_genes = 0
            family_sizes = [ ]
        elif "\t" in line:
            family_name = line
            family_sizes.append( 0 )
        else:
            family_sizes[ -1 ] += 1

exit( 0 )
