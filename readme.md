Composite Finder Searcher
=========================

Searches a composite finder file for composites with a cerain number of sequences per family.

Usage
-----

```bash
composite-finder-searcher [--min|--max|--avg|--sum] [<quantity>] [<file_name>]
```

* `min`: Filter by smallest family
* `max`: Filter by largest family
* `avg`: Filter by average family
* `sum`: Filter by total number of sequences (regardless of family)

Meta
----

```ini
type=script
language=python
host=bitbucket
```
